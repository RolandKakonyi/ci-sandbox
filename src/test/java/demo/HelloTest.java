package demo;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by roland.kakonyi@e2-online.com on 12/04/16.
 */
public class HelloTest {
    @Test
    public void testThisWillPass() throws Exception {
        assertTrue("this should be true", true);
    }

    @Test
    public void testYouShallNotPass() throws Exception {
        assertFalse("this should be false", false);
    }

    @Test
    public void testHello() throws Exception {
        assertEquals(new Hello().home(), "Hello World");
    }
}